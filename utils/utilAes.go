package utils

import (
	"crypto/cipher"
	"bytes"
	"crypto/aes"
	"encoding/base64"
)
/**
	https://code.csdn.net/snippets/1576203
	库里没有，要加进去

 */
type utilAes  struct{
	key []byte
	block cipher.Block
	size int
	enMode cipher.BlockMode
	deMode cipher.BlockMode
}

type UtilAes interface {
	Encrypt([]byte) string
	Decrypt(string) []byte
}
//加
func (self *utilAes) Encrypt(origData []byte) string {
	origData = PKCS7Padding(origData, self.size)
	crypted := make([]byte, len(origData))
	self.enMode.CryptBlocks(crypted, origData)
	return base64.StdEncoding.EncodeToString(crypted)
}
//解
func (self *utilAes) Decrypt(crypted string) []byte {
	bs,err:=base64.StdEncoding.DecodeString(crypted)
	if err!=nil {
		return []byte(crypted)
	}
	origData := make([]byte, len(bs))
	self.deMode.CryptBlocks(origData, bs)
	origData = PKCS7UnPadding(origData,self.size)
	return origData
}


func PKCS7Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}
func PKCS7UnPadding(plantText []byte, blockSize int) []byte {
	length := len(plantText)
	unpadding := int(plantText[length-1])
	return plantText[:(length - unpadding)]
}

func NewUtilAes(k []byte) UtilAes{
	b,_:=aes.NewCipher(k)
	return &utilAes{
		key:k,
		block:b,
		size:b.BlockSize(),
		enMode:cipher.NewECBEncrypter(b),
		deMode:cipher.NewECBDecrypter(b)}
}

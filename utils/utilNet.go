package utils

import (
)
import (
	"net"
)

/**
	得到本机ip
 */
func GetLocalIp() string{
	addrs, _ := net.InterfaceAddrs()
	for _, a := range addrs {
		ipnet, ok := a.(*net.IPNet);
		if  ok && !ipnet.IP.IsLoopback() && ipnet.IP.IsGlobalUnicast(){
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String();
			}
		}
	}
	return "未知IP"
}






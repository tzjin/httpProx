package main

import (
	"fmt"
	"net/http"
	"io"
	"io/ioutil"
	"strings"
)

var con *configs.Config
var utilAes utils.UtilAes

func main() {
	con = configs.NewConfig()
	utilAes = utils.NewUtilAes([]byte("jiankangwuyouabc"))
	if err := recover(); err != nil {
		fmt.Errorf("%s", err)
	}
	http.HandleFunc("/", handler)
	fmt.Println("Start serving "+utils.GetLocalIp()+":8888")
	http.ListenAndServe(":8888", nil)
}


/**
	代理
 */
func handler(w http.ResponseWriter, r *http.Request) {
	if !con.IsHttp {
		return;
	}
	var sb []string//用于打印的
	var contType string
	url := r.URL.Scheme + "://" + r.Host + r.URL.Path
	if len(r.URL.RawQuery) != 0 {
		url += "?" + r.URL.RawQuery
	}
	newReq, err := http.NewRequest(r.Method, url, r.Body)
	if err != nil {
		panic(err)
	}
	if !con.Response{
		//请求信息
		sb = append(sb, "\n" + r.Proto + " " + r.Method + " " + url)//HTTP/1.1 GET http://www.
	}
	for k, v := range r.Header {
		for _, vv := range v {
			newReq.Header.Add(k, vv)
			if k=="Content-Type" {
				contType = vv
			}
			if con.PrintHead && !con.Response{
				sb = append(sb, k + ":" + vv)//Content-Type:application/x-www-form-urlencoded;charset=utf-8
			}
		}
	}
	ioutil.NopCloser(r.Body)
	//var reqboy []byte
	if !con.Response {
		_, sb = toBytes(contType,sb, r.Body)//上传的内容
		//newReq.Body = utils.NewReader(reqboy)
	}else{
		//result, err := ioutil.ReadAll(r.Body)
		//if err != nil && err != io.EOF {
		//	panic(err)
		//}
		//newReq.Body = utils.NewReader(result)
	}



	//调用http请求
	resp, err := http.DefaultClient.Do(newReq)
	defer resp.Body.Close()
	if err != nil {
		panic(err)
	}

	//返回
	sb = append(sb, r.Proto + " " + resp.Status + " " + url)//HTTP/1.1 200 OK http://www
	for k, v := range resp.Header {
		for _, vv := range v {
			w.Header().Add(k, vv)
			if k=="Content-Type" {
				contType = vv
			}
			if con.PrintHead {
				sb = append(sb, k + ":" + vv)//Content-Length:0
			}
		}
	}
	w.WriteHeader(resp.StatusCode)
	resboy, sb := toBytes(contType,sb, resp.Body);
	print(sb)
	w.Write(resboy)
}

func toBytes(contType string,sb []string, r io.Reader) ([]byte, []string) {
	result, err := ioutil.ReadAll(r)
	resultStr:=string(result)
	if err != nil && err != io.EOF {
		panic(err)
	}
	var s []string
	if con.DecryptAes {
		if strings.Contains(contType,"text/plain") || strings.Contains(contType,"text/html"){
			if strings.HasPrefix(resultStr,"{") || strings.HasPrefix(resultStr,"[{") {
				s = append(sb, resultStr)
			}else{
				str := string(utilAes.Decrypt(resultStr))
				s = append(sb, str)
			}
		}else if strings.Contains(contType,"image/"){
			s = append(sb, "[图片]")
		}else{
			s = append(sb, resultStr)
		}
	}else{
		s = append(sb, resultStr)
	}
	return result, s
}

func print(sb []string) {
	for index := range sb {
		fmt.Println(sb[index])
	}
}
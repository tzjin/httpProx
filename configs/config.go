package configs

import (
	"os"
	"bufio"
	"fmt"
)

type Config struct {
	IsHttp bool
	PrintHead bool
	DecryptAes bool
	Response bool
}

func (this *Config) start() {
	this.IsHttp = true
	this.PrintHead = true
	this.DecryptAes = true
	this.Response = false
	reader := bufio.NewReader(os.Stdin)
	for true {
		reader.ReadLine()
		this.menu(reader)
	}
}

func (this *Config) menu(reader *bufio.Reader) {
	//this.IsHttp = false
	fmt.Println("请输入以下命令:")
	fmt.Print("http--接收http代理--")
	fmt.Println(this.IsHttp)
	fmt.Print("head--http头部打印开关--")
	fmt.Println(this.PrintHead)
	fmt.Print("aes --解密开关--")
	fmt.Println(this.DecryptAes)
	fmt.Print("res --是否只看返回--")
	fmt.Println(this.Response)
	fmt.Println("exit--退出")
	data, _, _ := reader.ReadLine()
	command := string(data)

	if command == "exit" {
		os.Exit(0)
	} else if command == "http"{
		this.IsHttp = true
		fmt.Println("http代理已开启")
	}else if command == "head"{
		this.PrintHead = !this.PrintHead
		fmt.Print("头部打印状态：")
		fmt.Println(this.PrintHead)
	}else if command == "aes"{
		this.DecryptAes = !this.DecryptAes
		fmt.Print("是否解密：")
		fmt.Println(this.DecryptAes)
	}else if command == "res"{
		this.Response = !this.Response
		fmt.Print("是否只看返回：")
		fmt.Println(this.Response)
	}else{
		this.menu(reader)
	}
}


func NewConfig() *Config {
	con := new(Config)
	go con.start()
	return con
}

